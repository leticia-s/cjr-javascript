/*	@autor: Letícia de Souza Soares 
	@ano 2019-2 
	*/

// atividade colocar vermelho onde estiver negativo e verde onde estiver positivo
let tabela2 = document.getElementById("minha-linda-tabela")
let num_itens = tabela2.tBodies[1].rows.length-1; 

for(let i=0; i<num_itens; i++){
	if( tabela2.tBodies[1].rows[i].cells[3].innerText >= 0){
		tabela2.tBodies[1].rows[i].classList.add("table-success")
	}else {
		tabela2.tBodies[1].rows[i].classList.add("table-danger")
	}
}


function soma(n){
	let soma = 0
	for(let i=0; i<num_itens; i++){
		soma += Number(n.tBodies[1].rows[i].cells[3].innerText)
	}
	return soma
}

let total = document.getElementById("tabela-total")
total.cells[1].innerText =soma(tabela2).toFixed(2)



//declarações para desafio
let dark_mode = document.getElementById("toggle-dark-mode")
let bg = document.querySelectorAll('.bg-light')
let text = document.querySelectorAll('.text-dark')
let btn = document.querySelectorAll('.btn-dark')
let text_l = document.querySelectorAll('.text-light')
let i,aux

dark_mode.addEventListener("click", function(){
	/*Na tentativa inicial: usei uma variavel booleana e depois usei remove e add, 
	mas ficou o dobro do codigo*/
	for(i=0; i<bg.length;i++){
		aux=bg[i]
			//toggle add a class se ela não estiver lá e remove se estiver
			aux.classList.toggle("bg-light")
			aux.classList.toggle("bg-dark")

		}
		for(i=0; i<text.length;i++){
			aux=text[i]
			aux.classList.toggle("text-dark")
			aux.classList.toggle("text-light")
		}
		for(i=0; i<text_l.length;i++){
			aux=text_l[i]
			aux.classList.toggle("text-dark")
			aux.classList.toggle("text-light")
		}
		for(i=0; i<btn.length;i++){
			aux=btn[i]
			aux.classList.toggle("btn-light")
			aux.classList.toggle("btn-dark")
		}
		
	});



